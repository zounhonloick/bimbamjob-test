import React, { useState, useEffect } from 'react';

const TheComponent = ({ instructions }) => {
  const [position, setPosition] = useState([0, 0, 'N']);

  useEffect(() => {
    const exec = () => {
      const orientationMap = {
        N: { R: 'E', L: 'W', F: [0, 1] },
        E: { R: 'S', L: 'N', F: [1, 0] },
        S: { R: 'W', L: 'E', F: [0, -1] },
        W: { R: 'N', L: 'S', F: [-1, 0] },
      };

      let [x, y, orientation] = position;

      for (let i = 0; i < instructions.length; i++) {
        const instruction = instructions[i];

        if (instruction === 'R' || instruction === 'L') {
          orientation = orientationMap[orientation][instruction];
        } else if (instruction === 'F') {
          const [dx, dy] = orientationMap[orientation][instruction];
          const newX = x + dx;
          const newY = y + dy;

          if (newX >= 0 && newX <= 4 && newY >= 0 && newY <= 4) {
            x = newX;
            y = newY;
          }
        }
      }

      setPosition([x, y, orientation]);
    };

    exec();
  }, []);

  return (
    <div>
      <p>
        Position: [{position[0]}, {position[1]}] et orientation {position[2]}
      </p>
    </div>
  );
};

export default TheComponent;
