import TheComponent from './TheComponent'
import React from 'react';
const App = () => {
  const instructions1 = 'LFRRFFLFRFF';
  const instructions2 = 'FFRLLRFRLF';

  return (
    <div>
      <TheComponent instructions={instructions1} />
      <TheComponent instructions={instructions2} />
    </div>
  );
};

export default App;
